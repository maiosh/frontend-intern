# Frontend intern test #


Prosilibyśmy Cię o wykonanie krótkiego zadania rekrutacyjnego, które pomoże nam sprawdzić Twoje umiejętności.
Chodzi o wykonanie prostego widgeta pogodowego, który będzie po prostu wyświetlał pogodę dla wybranego miasta (do wyboru z listy - Warszawa, Katowice, Wrocław, Gdańsk) oraz pokayzwał aktualny czas.
Domyślnie ma być wybrana Warszawa, a po zmianie wyboru ładujemy pogodę dla wybranego miasta.

Na początek zrób sobie prywatnego Forka tego repozytorium i daj nam do niego dostęp.

Dane należy pobierac z API OpenWeatherMap. Klucz do api to **56cbe5e8b526140b3cff697246ed7e10**, należy go podawać w requestach jako parametr APPID np. http://api.openweathermap.org/data/2.5/weather?q=Warsaw&APPID=56cbe5e8b526140b3cff697246ed7e10

Chcielibysmy, aby kod był napisany z wykorzystaniem ***EcmaScript 6*** oraz ***SASS***.

Projekt graficzny znajduje się w repozytorium. Nie musi być przełożony 1:1. Jeżeli da się coś zrobić lepiej / ładniej to zrób tak. Tego nie projektował grafik, tylko programista :)

Ikony pogody weź z kolekcji Material Deisgn Icons - https://materialdesignicons.com/

Wykorzystaj font Open Sans z Google Fonts.